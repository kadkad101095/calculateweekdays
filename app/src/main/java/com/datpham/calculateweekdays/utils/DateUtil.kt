package com.datpham.calculateweekdays.utils

import com.datpham.calculateweekdays.model.Day
import com.datpham.calculateweekdays.model.Holiday
import com.datpham.calculateweekdays.model.HolidayType
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.HashSet

class DateUtil {

    companion object {

        fun getWeekendBetweenTwoDates(startDate: Date, endDate: Date): HashSet<String> {
            val startCal = Calendar.getInstance()
            startCal.time = startDate
            val endCal: Calendar = Calendar.getInstance()
            endCal.time = endDate
            val result = HashSet<String>()

            //Return nothing if start and end are the same
            if (startCal.timeInMillis == endCal.timeInMillis) {
                return result
            }
            if (startCal.timeInMillis > endCal.timeInMillis) {
                startCal.time = endDate
                endCal.time = startDate
            }
            do {
                //excluding start date and end date
                startCal.add(Calendar.DAY_OF_MONTH, 1)
                if (startCal.timeInMillis >= endCal.timeInMillis) {
                    break
                }
                if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
                    || startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY
                ) {
                    val year = startCal.get(Calendar.YEAR)
                    val calendarMonth = startCal.get(Calendar.MONTH) + 1
                    val month =
                        if (calendarMonth < 10)
                            "0$calendarMonth"
                        else "$calendarMonth"
                    val calendarDay = startCal.get(Calendar.DAY_OF_MONTH)
                    val day = if (calendarDay < 10)
                        "0$calendarDay"
                    else "$calendarDay"
                    result.add("$year-$month-$day")
                }
            } while (startCal.timeInMillis < endCal.timeInMillis) //excluding end date
            return result
        }

        fun getFixedHolidaysBetweenTwoDates(
            startDate: Date,
            endDate: Date,
            holidays: ArrayList<Holiday>
        ): HashSet<String> {
            val startCal = Calendar.getInstance()
            startCal.time = startDate
            val endCal: Calendar = Calendar.getInstance()
            endCal.time = endDate

            val result = HashSet<String>()

            //Return nothing if start and end are the same
            if (startCal.timeInMillis == endCal.timeInMillis) {
                return result
            }
            if (startCal.timeInMillis > endCal.timeInMillis) {
                startCal.time = endDate
                endCal.time = startDate
            }

            val holidaySet = HashSet<String>()
            holidays.forEach { holiday ->
                //Filter fixed holiday only
                if (holiday.type == HolidayType.FIXED.value) {
                    holiday.date?.let { holidaySet.add(it) }
                }
            }

            do {
                //excluding start date and end date
                startCal.add(Calendar.DAY_OF_MONTH, 1)
                if (startCal.timeInMillis >= endCal.timeInMillis) {
                    break
                }
                val year = startCal.get(Calendar.YEAR)
                val calendarMonth = startCal.get(Calendar.MONTH) + 1
                val month =
                    if (calendarMonth < 10)
                        "0$calendarMonth"
                    else "$calendarMonth"
                val calendarDay = startCal.get(Calendar.DAY_OF_MONTH)
                val day = if (calendarDay < 10)
                    "0$calendarDay"
                else "$calendarDay"
                val checkKey = "$month-$day"
                if (holidaySet.contains(checkKey)) {
                    result.add("$year-$month-$day")
                }
            } while (startCal.timeInMillis < endCal.timeInMillis) //excluding end date
            return result
        }


        fun getDynamicHolidaysBetweenTwoDates(
            startDate: Date,
            endDate: Date,
            holidays: ArrayList<Holiday>
        ): HashSet<String> {
            val startCal = Calendar.getInstance()
            startCal.time = startDate
            val endCal: Calendar = Calendar.getInstance()
            endCal.time = endDate

            val result = HashSet<String>()

            //Return nothing if start and end are the same
            if (startCal.timeInMillis == endCal.timeInMillis) {
                return result
            }
            if (startCal.timeInMillis > endCal.timeInMillis) {
                startCal.time = endDate
                endCal.time = startDate
            }

            val holidaySet = HashSet<String>()
            holidays.forEach { holiday ->
                //Filter dynamic holiday only
                if (holiday.type == HolidayType.FLEX.value) {
                    holiday.date?.let { holidaySet.add(it) }
                }
            }

            do {
                //excluding start date and end date
                startCal.add(Calendar.DAY_OF_MONTH, 1)
                if (startCal.timeInMillis >= endCal.timeInMillis) {
                    break
                }
                val year = startCal.get(Calendar.YEAR)
                val calendarMonth = startCal.get(Calendar.MONTH) + 1
                val month =
                    if (calendarMonth < 10)
                        "0$calendarMonth"
                    else "$calendarMonth" //Make sure have zero digit when lt 10
                val calendarDay = startCal.get(Calendar.DAY_OF_MONTH)
                var day = if (calendarDay < 10)
                    "0$calendarDay"
                else "$calendarDay" //Make sure have zero digit when lt 10
                val checkKey = "$month-$day"
                if (holidaySet.contains(checkKey)) {
                    if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
                        val newDay = calendarDay + 2
                        day = if (newDay < 10)
                            "0$newDay"
                        else "$newDay"
                        //Check if next monday is already a holiday
                        val checkMonday = "$month-$day"
                        if (holidaySet.contains(checkMonday)) {
                            //It is a holiday -> adjust one more day
                            val nextDay = newDay + 1
                            day = if (nextDay < 10)
                                "0$nextDay"
                            else "$nextDay"
                        }
                    }
                    if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                        val newDay = calendarDay + 1
                        day = if (newDay < 10)
                            "0$newDay"
                        else "$newDay"
                        //Check if next monday is already a holiday
                        val checkMonday = "$month-$day"
                        if (holidaySet.contains(checkMonday)) {
                            //It is a holiday -> adjust one more day
                            val nextDay = newDay + 1
                            day = if (nextDay < 10)
                                "0$nextDay"
                            else "$nextDay"
                        }
                    }
                    result.add("$year-$month-$day")
                }
            } while (startCal.timeInMillis < endCal.timeInMillis) //excluding end date
            return result
        }

        fun getSpecialHolidaysBetweenTwoDates(
            startDate: Date,
            endDate: Date,
            holidays: ArrayList<Holiday>
        ): HashSet<String> {
            val startCal = Calendar.getInstance()
            startCal.time = startDate
            val endCal: Calendar = Calendar.getInstance()
            endCal.time = endDate

            val result = HashSet<String>()

            val holidayMap = HashMap<Int, Array<Int>>()
            holidays.forEach { holiday ->
                if (holiday.type == HolidayType.SPECIAL.value
                    && holiday.order != null
                    && holiday.dayOfWeek != null
                    && holiday.month != null
                ) {
                    val order = holiday.order //Order of dayOfWeek in month (1st, 2nd)
                    val dayOfWeek = when (holiday.dayOfWeek) {
                        Day.SUNDAY.key -> Day.SUNDAY.value
                        Day.MONDAY.key -> Day.MONDAY.value
                        Day.TUESDAY.key -> Day.TUESDAY.value
                        Day.WEDNESDAY.key -> Day.WEDNESDAY.value
                        Day.THURSDAY.key -> Day.THURSDAY.value
                        Day.FRIDAY.key -> Day.FRIDAY.value
                        Day.SATURDAY.key -> Day.SATURDAY.value
                        else -> Day.SUNDAY.value
                    }
                    val month = holiday.month.toIntOrNull() ?: 0
                    holidayMap[month] = arrayOf(order, dayOfWeek)
                }
            }

            //Return nothing if start and end are the same
            if (startCal.timeInMillis == endCal.timeInMillis) {
                return result
            }
            if (startCal.timeInMillis > endCal.timeInMillis) {
                startCal.time = endDate
                endCal.time = startDate
            }
            do {
                //excluding start date and end date
                startCal.add(Calendar.DAY_OF_MONTH, 1)
                if (startCal.timeInMillis >= endCal.timeInMillis) {
                    break
                }
                val year = startCal.get(Calendar.YEAR)
                val calendarMonth = startCal.get(Calendar.MONTH) + 1
                val month =
                    if (calendarMonth < 10)
                        "0$calendarMonth"
                    else "$calendarMonth"
                val calendarDay = startCal.get(Calendar.DAY_OF_MONTH)
                val day = if (calendarDay < 10)
                    "0$calendarDay"
                else "$calendarDay"
                if (holidayMap.contains(calendarMonth)) {
                    val listValue = holidayMap[calendarMonth]
                    val order = listValue!![0]
                    val dayOfWeek = listValue[1]

                    if (startCal.get(Calendar.DAY_OF_WEEK_IN_MONTH) == order
                        && startCal.get(Calendar.DAY_OF_WEEK) == dayOfWeek
                    ) {
                        result.add("$year-$month-$day")
                    }
                }
            } while (startCal.timeInMillis < endCal.timeInMillis) //excluding end date
            return result
        }

        fun getAllDaysBetweenTwoDates(
            startDate: Date,
            endDate: Date
        ): HashSet<String> {
            val startCal = Calendar.getInstance()
            startCal.time = startDate
            val endCal: Calendar = Calendar.getInstance()
            endCal.time = endDate

            val result = HashSet<String>()

            //Return nothing if start and end are the same
            if (startCal.timeInMillis == endCal.timeInMillis) {
                return result
            }
            if (startCal.timeInMillis > endCal.timeInMillis) {
                startCal.time = endDate
                endCal.time = startDate
            }

            do {
                //excluding start date and end date
                startCal.add(Calendar.DAY_OF_MONTH, 1)
                if (startCal.timeInMillis >= endCal.timeInMillis) {
                    break
                }
                val year = startCal.get(Calendar.YEAR)
                val calendarMonth = startCal.get(Calendar.MONTH) + 1
                val month =
                    if (calendarMonth < 10)
                        "0$calendarMonth"
                    else "$calendarMonth"
                val calendarDay = startCal.get(Calendar.DAY_OF_MONTH)
                val day = if (calendarDay < 10)
                    "0$calendarDay"
                else "$calendarDay"

                //Add all day
                result.add("$year-$month-$day")
            } while (startCal.timeInMillis < endCal.timeInMillis) //excluding end date
            return result
        }

    }
}