package com.datpham.calculateweekdays.utils

import android.content.Context
import java.text.SimpleDateFormat
import java.util.*

fun Context.readJsonAsset(fileName: String): String {
    val inputStream = assets.open(fileName)
    val size = inputStream.available()
    val buffer = ByteArray(size)
    inputStream.read(buffer)
    inputStream.close()
    return String(buffer, Charsets.UTF_8)
}

fun String.toDate(format: String, locale: Locale = Locale.getDefault()): Date {
    val format = SimpleDateFormat(format, locale)
    return format.parse(this)
}

fun Date.toString(format: String, locale: Locale = Locale.getDefault()): String {
    val formatter = SimpleDateFormat(format, locale)
    return formatter.format(this)
}