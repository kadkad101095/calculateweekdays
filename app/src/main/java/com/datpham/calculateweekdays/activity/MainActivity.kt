package com.datpham.calculateweekdays.activity

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import com.datpham.calculateweekdays.R
import com.datpham.calculateweekdays.dialog.NotyDialog
import com.datpham.calculateweekdays.model.Holiday
import com.datpham.calculateweekdays.model.HolidayType
import com.datpham.calculateweekdays.utils.DateUtil
import com.datpham.calculateweekdays.utils.toDate
import com.datpham.calculateweekdays.viewmodel.MyViewModel
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

const val TAG_STARTDATE = "TAG_STARTDATE"
const val TAG_ENDDATE = "TAG_ENDDATE"
const val DATE_FORMAT = "yyyy-MM-dd"

class MainActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener {

    private lateinit var myViewModel: MyViewModel
    private lateinit var datePickerDialog: DatePickerDialog
    private val holidaySet = HashSet<String>()
    private val listHoliday = ArrayList<Holiday>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        myViewModel = ViewModelProvider(this)[MyViewModel::class.java]
        initViews()
        initObserver()
    }

    private fun initViews() {

        val now = Calendar.getInstance()
        datePickerDialog = DatePickerDialog.newInstance(
            this,
            now[Calendar.YEAR],
            now[Calendar.MONTH],
            now[Calendar.DAY_OF_MONTH]
        )

        setWeekdayResult() //Defaul show waiting

        setStartDate(
            now[Calendar.YEAR],
            now[Calendar.MONTH] + 1,
            now[Calendar.DAY_OF_MONTH]
        )

        setEndDate(
            now[Calendar.YEAR],
            now[Calendar.MONTH] + 1,
            now[Calendar.DAY_OF_MONTH]
        )

        tvStartDate.setOnClickListener {
            datePickerDialog.show(supportFragmentManager, TAG_STARTDATE)
        }

        tvEndDate.setOnClickListener {
            datePickerDialog.show(supportFragmentManager, TAG_ENDDATE)
        }

        btnCalculate.setOnClickListener {
            if (!tvStartDate.text.toString().toDate(DATE_FORMAT)
                    .before(tvEndDate.text.toString().toDate(DATE_FORMAT))
            ) {
                NotyDialog(this, getString(R.string.validate_date_string))
                return@setOnClickListener
            }
            showLoading(true)
            myViewModel.getListHoliday(this, tvStartDate.text.toString(), tvEndDate.text.toString())
        }

    }

    private fun initObserver() {
        myViewModel.holidayData.observe(this) { holidayData ->
            showLoading(false)

            holidaySet.clear()
            listHoliday.clear()
            listHoliday.addAll(holidayData.hodidayResponse.data)

            val listAllDay = getAllDays(holidayData.startDate, holidayData.endDate)
            val listWeekend = getWeekendHolidays(holidayData.startDate, holidayData.endDate)
            val listFixedHoliday = getFixedHolidays(holidayData.startDate, holidayData.endDate)
            val listFlexHoliday = getDynamicHolidays(holidayData.startDate, holidayData.endDate)
            val listSpecialHoliday = getSpecialHolidays(holidayData.startDate, holidayData.endDate)

            //Use set to merge holidays that same date
            holidaySet.addAll(listWeekend)
            holidaySet.addAll(listFixedHoliday)
            holidaySet.addAll(listFlexHoliday)
            holidaySet.addAll(listSpecialHoliday)

            //Weekday is the day not in holidays
            setWeekdayResult(
                listAllDay.size,
                listWeekend.size,
                listFixedHoliday.size,
                listFlexHoliday.size,
                listSpecialHoliday.size,
                listAllDay.size - holidaySet.size
            )
        }
    }

    //Callback when select date
    override fun onDateSet(view: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        when (view?.tag) {
            TAG_STARTDATE -> setStartDate(year, monthOfYear + 1, dayOfMonth)
            TAG_ENDDATE -> setEndDate(year, monthOfYear + 1, dayOfMonth)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setStartDate(year: Int, month: Int, day: Int) {
        val strMonth = if (month < 10)
            "0$month"
        else "$month"
        val strDay = if (day < 10)
            "0$day"
        else "$day"
        tvStartDate.text = "$year-$strMonth-$strDay"
    }

    @SuppressLint("SetTextI18n")
    private fun setEndDate(year: Int, month: Int, day: Int) {
        val strMonth = if (month < 10)
            "0$month"
        else "$month"
        val strDay = if (day < 10)
            "0$day"
        else "$day"
        tvEndDate.text = "$year-$strMonth-$strDay"
    }

    private fun setWeekdayResult(
        all: Int = 0,
        weekend: Int = 0,
        fixed: Int = 0,
        flex: Int = 0,
        special: Int = 0,
        weekDays: Int = 0
    ) {
        tvAllday.text = getString(R.string.all_day, if (all > 0) "$all" else "-")
        tvWeekend.text = getString(R.string.weekends, if (weekend > 0) "$weekend" else "-")
        tvFixedHoliday.text = getString(R.string.fixed_holiday, if (fixed > 0) "$fixed" else "-")
        tvFlexHoliday.text = getString(R.string.flex_holiday, if (flex > 0) "$flex" else "-")
        tvSpecialHoliday.text =
            getString(R.string.special_holiday, if (special > 0) "$special" else "-")
        tvWeekDay.text = getString(R.string.weekday, if (weekDays > 0) "$weekDays" else "-")
    }

    private fun getAllDays(startDate: String, endDate: String): HashSet<String> {
        return DateUtil.getAllDaysBetweenTwoDates(
            startDate.toDate(DATE_FORMAT),
            endDate.toDate(DATE_FORMAT)
        )
    }

    private fun getWeekendHolidays(startDate: String, endDate: String): HashSet<String> {
        return DateUtil.getWeekendBetweenTwoDates(
            startDate.toDate(DATE_FORMAT),
            endDate.toDate(DATE_FORMAT)
        )
    }

    private fun getFixedHolidays(startDate: String, endDate: String): HashSet<String> {
        return DateUtil.getFixedHolidaysBetweenTwoDates(
            startDate.toDate(DATE_FORMAT),
            endDate.toDate(DATE_FORMAT),
            listHoliday
        )
    }

    private fun getDynamicHolidays(startDate: String, endDate: String): HashSet<String> {
        return DateUtil.getDynamicHolidaysBetweenTwoDates(
            startDate.toDate(DATE_FORMAT),
            endDate.toDate(DATE_FORMAT),
            listHoliday
        )
    }

    private fun getSpecialHolidays(startDate: String, endDate: String): HashSet<String> {
        return DateUtil.getSpecialHolidaysBetweenTwoDates(
            startDate.toDate(DATE_FORMAT),
            endDate.toDate(DATE_FORMAT),
            listHoliday
        )
    }

    private fun showLoading(isLoading: Boolean) {
        viewLoading.isVisible = isLoading
    }

}