package com.datpham.calculateweekdays.dialog

import android.app.Dialog
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import com.datpham.calculateweekdays.R

class NotyDialog(context: Context, message: String?) {
    init {
        with(Dialog(context)) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawableResource(R.color.transparent)
            setContentView(R.layout.dialog_noty)
            val tvContent = findViewById<TextView>(R.id.tvContent)
            tvContent.text = message
            val btnClose = findViewById<AppCompatButton>(R.id.btnClose)
            btnClose.setOnClickListener { cancel() }
            this
        }.also { dialog ->
            dialog.window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }.show()
    }
}