package com.datpham.calculateweekdays.model

data class HolidayResponse(val data: ArrayList<Holiday>)
