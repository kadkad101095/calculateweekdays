package com.datpham.calculateweekdays.model

data class HolidayData(
    val startDate: String,
    val endDate: String,
    val hodidayResponse: HolidayResponse
)