package com.datpham.calculateweekdays.model

import java.util.*

enum class HolidayType(val value: String) {
    FIXED("FIXED"),
    FLEX("FLEX"),
    SPECIAL("SPECIAL")
}

enum class Day(val key: String, val value: Int) {
    SUNDAY("SUNDAY", Calendar.SUNDAY),
    MONDAY("MONDAY", Calendar.MONDAY),
    TUESDAY("TUESDAY", Calendar.TUESDAY),
    WEDNESDAY("WEDNESDAY", Calendar.WEDNESDAY),
    THURSDAY("THURSDAY", Calendar.THURSDAY),
    FRIDAY("FRIDAY", Calendar.FRIDAY),
    SATURDAY("SATURDAY", Calendar.SATURDAY)
}

data class Holiday(
    val name: String,
    val date: String?,
    val order: Int?,
    val dayOfWeek: String?,
    val month: String?,
    val type: String
)
