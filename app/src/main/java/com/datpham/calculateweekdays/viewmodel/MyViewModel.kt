package com.datpham.calculateweekdays.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.datpham.calculateweekdays.model.HolidayData
import com.datpham.calculateweekdays.model.HolidayResponse
import com.datpham.calculateweekdays.utils.readJsonAsset
import com.google.gson.Gson
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class MyViewModel(application: Application) : AndroidViewModel(application) {

    private val gson = Gson()
    val holidayData = MutableLiveData<HolidayData>()

    fun launch(block: suspend () -> Unit): Job {
        return viewModelScope.launch {
            block()
        }
    }

    fun getListHoliday(context: Context, startDate: String, endDate: String) = launch {
        val response =
            gson.fromJson(context.readJsonAsset("holidays_1.json"), HolidayResponse::class.java)
        holidayData.value = HolidayData(startDate, endDate, response)
    }

}